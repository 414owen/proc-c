#include <stdlib.h>

#include "vect.h"

// create some vector types using the vector macros
declare_vect_funcs(char, char);
declare_vect_funcs(ptr, void*);
