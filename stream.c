#define _GNU_SOURCE

#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "header.h"
#include "util.h"
#include "vect.h"

// read into an expanding buffer until a condition is met
char *predicated_read(int file, size_t go_back, char* (*pred) (char*, size_t),
		vect_char **vp) {
	vect_char *v = vect_char_init(block_size);
	*vp = v;
	while (true) {
		char *c = v->data + v->length;
		size_t space = v->limit - v->length;

		size_t n = read(file, c, space);
		if (n == -1) thread_error("Error reading stream");
		v->length += n;
		char *d = max(c - go_back, v->data);

		// return ptr to last char read
		char *predend = pred(d, c + n - d);
		if (predend != NULL) return predend;
		if (n == 0) return NULL;
		if (v->length * 2 > v->limit) {
			vect_char_double(v);
		}
	}
}

// writes entire buffer, returns true on success
bool write_buffer(int sock, char *buf, size_t len) {
	// write to remote
	ssize_t n;
	size_t written = 0;
	while ((n = write(sock, buf + written, len - written))) {
		if (n < 0) {
			perror("write buffer");
			break;
		}
		written += n;
		if (written == len) return true;
	}
	return false;
}

// forward all the things from one socket to another using kernel-level
// (and most likely hardware-level) zero-copy socket splicing
bool write_through(int sfd, int dfd) {
	int filedes[2];
	if (pipe(filedes) < 0) {
        perror("pipe");
		thread_error("");
    }
	ssize_t to_copy = 0;

	while (true) {
		ssize_t n;
		n = splice(sfd, NULL, filedes[1], NULL, block_size, SPLICE_F_MOVE | SPLICE_F_MORE)
		if (n == 0) {
			if (fcntl(sfd, F_GETFD)) close(sfd);
			if (fcntl(dfd, F_GETFD)) close(dfd);
		}
		to_copy += n;
		n = splice(filedes[0], NULL, dfd, NULL, to_copy, SPLICE_F_MOVE | SPLICE_F_MORE);
		to_copy -= n;
	}
}
