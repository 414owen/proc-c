#include <netinet/in.h>
#include <sys/stat.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <unistd.h>

#include "blocklist.h"
#include "http.h"
#include "header.h"
#include "socket.h"
#include "stream.h"

const char *get = "GET";
const char *post = "POST";
const char *delete = "DELETE";
const char *index_path = "index.html";
long int index_len;
int webui_sock;

const char *head_err = "couldn't send headers to webui";

// handle a request to the webui endpoint
void* handle_webui(void *args) {
	printf("Got webui request\n");
	int sock = *((int*) args);
	http_t http = stream_http(sock);

	if (strcmp(http.method, get) == 0) {
		if (strstr(http.url, "blocklist")) {
			// serve the (newline-separated) blocklist
			printf("blocklist request\n");
			vect_char buf = blocklist_serialise();
			if (!(send_okay(sock, http.version) &&
						send_content_length(sock, buf.length) &&
						send_end_headers(sock)))
				thread_error(head_err);
			if (buf.length > 0)
				if (!write_buffer(sock, buf.data, buf.length))
					thread_error("couldn't write webui JSON");
		} else {
			// serve the website
			printf("index request\n");
			int index = open(index_path, O_RDONLY);
			FILE *fp = fopen(index_path, "r");
			fseek(fp, 0L, SEEK_END);
			index_len = ftell(fp);
			fclose(fp);
			if (!(send_okay(sock, http.version) &&
						send_content_length(sock, index_len) &&
						send_end_headers(sock)))
				thread_error(head_err);
			if (!write_through(index, sock))
				thread_error("couldn't write file");
		}
	} else {
		char *url = get_header(http.headers, "Block");
		printf("block header found\n");
		if (url) {
			if (strcmp(http.method, delete) == 0) {
				// delete host from blocklist
				printf("removing %s from blocklist\n", url);
				blocklist_remove(url);
			} else {
				// add host to blocklist
				printf("adding %s to blocklist\n", url);
				blocklist_add(url);
			}
			printf("done\n");
			if (!(send_okay(sock, http.version) &&
						send_end_headers(sock)))
				thread_error(head_err);
		}
	}


	free(args);
	close(sock);
	printf("webui thread quitting\n");
	return NULL;
}

// Thread for accepting webui requests
void* run_webui(void *args) {
	int sockno = bound_sock(8080);
	listen(sockno, 5);
	FILE *fp = fopen(index_path, "r");
	fseek(fp, 0L, SEEK_END);
	index_len = ftell(fp);
	fclose(fp);
	struct sockaddr_in cli_addr;
	socklen_t clilen = sizeof(cli_addr);
	printf("Web UI listening on 127.0.0.1:8080\n");
	while (true) {
		int *args = malloc(sizeof(int));
		*args = accept(sockno, (struct sockaddr *) &cli_addr, &clilen);
		pthread_t handle;
		pthread_create(&handle, NULL, handle_webui, args);
	}
}
