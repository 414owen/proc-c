#pragma once

#include <stdlib.h>

#include "util.h"

// macros to define vector types and functions to manipulate them
// who says C isn't good at polymorphism?

#define declare_vect(name, t) \
	typedef struct { \
		size_t limit; \
		size_t length; \
		t *data; \
	} vect_ ## name;

#define declare_vect_funcs(name, t) \
	void vect_ ## name ## _push(vect_ ## name *v, t el) { \
		if (v->length >= v->limit) { \
			size_t new_limit = (v->limit + v->length); \
			size_t new_size = new_limit * sizeof(t); \
			v->data = realloc(v->data, new_size); \
			v->limit = new_limit; \
		} \
		v->data[v->length++] = el; \
	} \
\
	t vect_ ## name ## _pop(vect_ ## name *v) {\
		return v->data[--v->length]; \
	} \
\
	vect_ ## name *vect_ ## name ## _init(size_t size) { \
		size = max(size, 2); \
		vect_ ## name *result = malloc(sizeof(vect_ ## name)); \
		result->limit = size; \
		result->length = 0; \
		result->data = malloc(sizeof(t) * size); \
		return result; \
	} \
\
	vect_ ## name vect_ ## name ## _stack_init(size_t size) { \
		size = max(size, 2); \
		vect_ ## name res; \
		res.limit = size; \
		res.length = 0; \
		res.data = malloc(sizeof(t) * size); \
		return res; \
	} \
\
	void vect_ ## name ## _finalise(vect_ ## name *v) { \
		v->data = realloc(v->data, v->length * sizeof(t)); \
		v->limit = v->length; \
	} \
\
	size_t vect_ ## name ## _double(vect_ ## name *v) { \
		v->limit *= 2; \
		v->data = realloc(v->data, v->limit * sizeof(t)); \
		return v->limit; \
	}

#define predeclare_vect_funcs(name, t) \
	void vect_ ## name ## _push(vect_ ## name *, t); \
	t vect_ ## name ## _pop(vect_ ## name *); \
	vect_ ## name *vect_ ## name ## _init(size_t); \
	vect_ ## name vect_ ## name ## _stack_init(size_t); \
	void vect_ ## name ## _finalise(vect_ ## name *); \
	size_t vect_ ## name ## _double(vect_ ## name *v); \

declare_vect(char, char);
predeclare_vect_funcs(char, char);
declare_vect(ptr, void*);
predeclare_vect_funcs(ptr, void*);
