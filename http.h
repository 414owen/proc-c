#include <stdio.h>

#include "vect.h"
#include "header.h"

typedef struct {
	char *method;
	char *node;
	char *port;
	char *version;
	char *url;
	char *body;
	vect_char *data;
	size_t http_len;
	vect_header headers;
} http_t;

void free_http_t(http_t s);
http_t parse_http(vect_char *v, char *head_end);
http_t stream_http(int stream);
void fprint_http(FILE *file, http_t http);
bool send_okay(int dest, char *version);
bool send_end_headers(int dest);
bool send_content_length(int dest, size_t len);
bool send_redirect(int dest, char *url);
