#pragma once

#include <pthread.h>

#include "vect.h"

extern vect_ptr blocklist;
pthread_mutex_t blocklist_lock;

void blocklist_init(void);
bool blocked(char *host);
void blocklist_add(char *n);
vect_char blocklist_serialise();
void blocklist_remove(char *host);
