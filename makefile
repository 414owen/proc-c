files = http.c socket.c proxy.c util.c stream.c vect.c header.c blocklist.c webui.c
headers = header.h http.h socket.h stream.h util.h vect.h blocklist.h webui.h
CC = gcc

proxy: $(files) $(headers)
	$(CC) -Wall -g -std=c11 -pthread -o proxy $(files)
