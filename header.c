#define _GNU_SOURCE

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "header.h"
#include "util.h"
#include "vect.h"

declare_vect_funcs(header, header);

// header key comparator for qsort
int key_sort(const void *a, const void *b) {
	return strcmp(((header*) a)->key, ((header*) b)->key);
}

// parse vect_header from raw string
vect_header parse_headers(char *str, char *end) {
	vect_header headers = vect_header_stack_init(3);
	header h;
	// so we can find the last '\r'
	end += 2;
	while (str < end) {
		char *colon = memchr(str, ':', end - str);
		h.key = extract_str(str, colon - 1);
		/* printf("Key: %s\n", h.key); */
		char *endl = memmem(colon, end - colon, "\r\n", 2);
		h.value = extract_str(colon + 2, endl - 1);
		/* printf("Value: %s\n", h.value); */
		vect_header_push(&headers, h);
		str = endl + 2;
	}
	qsort(headers.data, headers.length, sizeof(header), key_sort);
	return headers;
}

// binary search for header
char* get_header(vect_header v, char *key) {
	size_t curs = 0;
	for (size_t h = v.length / 2; h > 0; h /= 2) {
		while (curs + h < v.length && strcmp(v.data[h + curs].key, key) <= 0)
			curs += h;
	}
	header head = v.data[curs];
	if (strcmp(head.key, key) == 0) return head.value;
	return NULL;
}
