#pragma once

#include <stdbool.h>

#include "vect.h"

bool write_buffer(int sock, char *buf, size_t len);
char *predicated_read(int file, size_t go_back, char* (*pred) (char*, size_t),
		vect_char **vp);
bool write_through(int sfd, int dfd);
