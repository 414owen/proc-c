#pragma once

#include <stdarg.h>
#include <stdbool.h>

#define min(a, b) ((a) < (b) ? (a) : (b))
#define max(a, b) ((a) > (b) ? (a) : (b))

extern int block_size;
extern int cachedir;
void init_util(void);
void prox_error(const char* error, ...);
char* extract_str(char *start, char *end);
void thread_error(const char* error, ...);
