#define _GNU_SOURCE

#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

#include "http.h"
#include "util.h"
#include "stream.h"

struct addrinfo hints;
const static int one = 1;

// get a socket set up just how I like it
int getsock(void) {
	int sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (sock < 0) thread_error("ERROR opening socket");
	setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, (const void*) &one, sizeof(int));
	setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (const void*) &one, sizeof(int));
	return sock;
}

// get a socket bound to a port
int bound_sock(int port) {

	int sockno = getsock();
	struct sockaddr_in serv_addr;
	memset((char *) &serv_addr, 0, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(port);

	if (bind(sockno, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
		thread_error("ERROR binding socket to port %d", port);

	return sockno;
}

// get a socket connected to a remote
int connected_sock(char *node, char *port) {
	int sock = getsock();
	struct addrinfo *i;
	if (getaddrinfo(node, port, &hints, &i))
		thread_error("host lookup of %s failed", node);
	while (i) {
		if (connect(sock, i->ai_addr, i->ai_addrlen) >= 0) return sock;
			/* fprintf(stderr, "trying next addrinfo for %s on %s\n", node, port); */
		i = i->ai_next;
	}
	freeaddrinfo(i);
	return -1;
}

