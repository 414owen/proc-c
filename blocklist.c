#include <pthread.h>
#include <stdio.h>
#include <string.h>

#include "vect.h"

vect_ptr blocklist;
size_t chars = 0;
pthread_mutex_t blocklist_lock;

void blocklist_init(void) {
	blocklist = vect_ptr_stack_init(64);
	pthread_mutex_init(&blocklist_lock, NULL);
}

// string comparator for qsort
int stringcmp(const void *a, const void *b) {
	return strcmp(*((const char **) a), *((const char**) b));
}

void sort_blocklist() {
	qsort(blocklist.data, blocklist.length, sizeof(void*), stringcmp);
}

void print_blocklist() {
	pthread_mutex_lock(&blocklist_lock);
	for (int i = 0; i < blocklist.length; i++) {
		puts(blocklist.data[i]);
		putchar('\n');
	}
	pthread_mutex_unlock(&blocklist_lock);
}

// binary search the sorted list of strings
bool blocklist_search(char *host, size_t *res) {
	size_t n = 0;
	for (size_t h = blocklist.length; h > 0; h /= 2) {
		while (n + h < blocklist.length && strcmp(blocklist.data[n + h], host) <= 0)
			n += h;
	}
	*res = n;
	return blocklist.length > 0 && strcmp(blocklist.data[n], host) == 0;
}

// remove item from blocklist
void blocklist_remove(char *host) {
	pthread_mutex_lock(&blocklist_lock);
	size_t n;
	if (blocklist_search(host, &n)) {
		for (; n < blocklist.length - 1; n++) {
			blocklist.data[n] = blocklist.data[n + 1];
		}
		blocklist.length--;
	}
	chars -= strlen(host);
	pthread_mutex_unlock(&blocklist_lock);
}

// format blocklist as newline-separated string
vect_char blocklist_serialise() {
	pthread_mutex_lock(&blocklist_lock);
	size_t buf_size = chars + blocklist.length;
	vect_char r = vect_char_stack_init(buf_size);
	char *res = r.data;
	char *c = res;
	for (int i = 0; i < blocklist.length; i++) {
		char *str = blocklist.data[i];
		size_t size = strlen(str);
		memcpy(c, str, size);
		c += size;
		*c = '\n';
		c++;
	}
	pthread_mutex_unlock(&blocklist_lock);
	r.length = buf_size;
	r.limit = buf_size;
	return r;
}

// is this host in the blocklist?
bool blocked(char *host) {
	bool res;
	size_t n;
	pthread_mutex_lock(&blocklist_lock);
	res = blocklist_search(host, &n);
	pthread_mutex_unlock(&blocklist_lock);
	return res;
}

// add item to blocklist (but don't add duplicates)
void blocklist_add(char *n) {
	if (blocked(n)) return;
	pthread_mutex_lock(&blocklist_lock);
	vect_ptr_push(&blocklist, (void*) n);
	sort_blocklist();
	chars += strlen(n);
	pthread_mutex_unlock(&blocklist_lock);
	print_blocklist();
}
