#define _GNU_SOURCE

#include <dirent.h>
#include <errno.h>
#include <pthread.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <pwd.h>
#include <unistd.h>

int block_size = 0;
int cachedir;
const char *homedir;
char *cachedir_str;
const char *prog_name = "proc-c";

// initialise patnames and block size
void init_util(void) {
	block_size = getpagesize();
	printf("Detected page size of %d\n", block_size);
	if ((homedir = getenv("HOME")) == NULL) {
		homedir = getpwuid(getuid())->pw_dir;
	}
	const char *cache_str = ".cache";
	cachedir_str = malloc(strlen(homedir) + strlen(cache_str) + 
			strlen(prog_name) + 4);
	sprintf(cachedir_str, "%s/%s", homedir, cache_str);
	if (mkdir(cachedir_str, S_IRWXU)) {
		if (errno != EEXIST) perror("create .cache");
	}
	sprintf(cachedir_str + strlen(cachedir_str), "/%s/", prog_name);
	if (mkdir(cachedir_str, S_IRWXU)) {
		if (errno != EEXIST) perror("create cache dir");
	}
	cachedir = dirfd(opendir(cachedir_str));
}

// give up on the process
void prox_error(const char* error, ...) {
	va_list args;
	va_start(args, error);
	vfprintf(stderr, error, args);
	fputc('\n', stderr);
	va_end(args);
	exit(0);
}

// extract a malloc'ed string
char* extract_str(char *start, char *end) {
	char *res = malloc(end - start + 2);
	memcpy(res, start, end - start + 2);
	res[end - start + 1] = '\0';
	return res;
}

// give up on the thread
void thread_error(const char* error, ...) {
	va_list args;
	va_start(args, error);
	vfprintf(stderr, error, args);
	fputc('\n', stderr);
	va_end(args);
	pthread_exit(&block_size);
}
