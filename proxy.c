#include <fcntl.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <pthread.h>
#include <signal.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <unistd.h>

#include "blocklist.h"
#include "error.h"
#include "http.h"
#include "socket.h"
#include "stream.h"
#include "util.h"
#include "vect.h"
#include "webui.h"

#define HL "------------------------\n"

int sockno;
int one = 1;
char *redirect_url = "https://www.youtube.com/watch?v=dQw4w9WgXcQ";

struct tunnel_args {
	int src;
	int dst;
	size_t buf_len;
	char* buf;
};

void *tunnel(void *args) {
	signal(SIGPIPE, SIG_IGN);
	struct tunnel_args *a = (struct tunnel_args*) args;
	if (!write_through(a->src, a->dst))
		thread_error("Tunnel caved in");
	return a;
}

void* handle_connection(void *args) {

	signal(SIGPIPE, SIG_IGN);
	// [socket, thread number]
	int *a = (int*) args;
	int thread_num = a[1], client_sock = a[0];

	http_t http = stream_http(client_sock);
	printf("%d: %s %s\n", thread_num, http.method, http.url);
	if (blocked(http.node)) {
		printf("request to %s blocked\n", http.node);
		if (!(send_redirect(client_sock, redirect_url) &&
					send_end_headers(client_sock)))
			thread_error("Couldn't confirm https connection");
		goto barecleanup;
	}
	/* fprint_http(stdout, http); */

	bool https = strcmp(http.method, "CONNECT") == 0;

	bool cache = !https;
	char *cache_fn = NULL;
	int remote_sock;
	if (cache) {
		size_t fnlen = strlen(http.url);
		cache_fn = malloc(fnlen * 2 + 1);
		char *curs = cache_fn;
		for (size_t i = 0; i < fnlen; i++) {
			sprintf(curs, "%02x", http.url[i >> 1]);
			curs += 2;
		}
		*curs = '\0';
	}

	/* bool cached = false; */
	if ((remote_sock = connected_sock(http.node, http.port)) == -1)
		thread_error("couldn't connect to %s on %s", http.node, http.port);
	if (https) {
		/* printf("sending okay\n"); */
		if (!(send_okay(client_sock, http.version) &&
					send_end_headers(client_sock)))
			thread_error("Couldn't confirm https connection");
	} else {
		/* if (cache) { */
		/*     printf("Returning cached file: %s\n", cache_fn); */
		/*     remote_sock = openat(cachedir, cache_fn, O_RDONLY); */
		/*     if (remote_sock < 0) cached = false; */
		/*     else cached = true; */
		/* } else { */

		/* printf("%d: sending headers and stuff\n", thread_num); */
		if (!write_buffer(remote_sock, http.data->data, http.data->length))
			thread_error("Coundn't send initial payload to remote");
	}

	/* printf("%d: parroting from remote\n", thread_num); */
	struct tunnel_args *a1 = malloc(sizeof(struct tunnel_args));
	struct tunnel_args *a2 = malloc(sizeof(struct tunnel_args));
	char *buffers = malloc(block_size * 2);

	a1->src = client_sock;
	a1->dst = remote_sock;
	a1->buf_len = block_size;
	a1->buf = buffers;

	a2->src = remote_sock;
	a2->dst = client_sock;
	a2->buf_len = block_size;
	a2->buf = buffers + block_size;

	pthread_t h1;
	pthread_t h2;
	void *ret;
	pthread_create(&h1, NULL, tunnel, a1);
	pthread_create(&h2, NULL, tunnel, a2);
	pthread_join(h1, &ret);
	pthread_join(h2, &ret);

	free(buffers);

	/* printf("Thread %d stopped\n\n", thread_num); */
	if (fcntl(remote_sock, F_GETFD)) close(remote_sock);
barecleanup:
	if (fcntl(client_sock, F_GETFD)) close(client_sock);
	free(args);
	free_http_t(http);
	// stop gcc complaint...
	return &one;
}

void cleanup() {
	fprintf(stderr, "Cleanup\n");
	close(sockno);
	close(webui_sock);
}

int main(int argc, char *argv[]) {

	if (argc < 2) {
		fprintf(stderr,"ERROR, no port provided\n");
		exit(1);
	}

	if (sockno < 0)
		thread_error("ERROR opening socket");

	int portno = atoi(argv[1]);
	printf("Starting proxy on port %d\n", portno);
	init_util();
	blocklist_init();
	sockno = bound_sock(portno);

	atexit(cleanup);
	signal(SIGPIPE, SIG_IGN);
	signal(SIGINT, cleanup);
	signal(SIGTERM, cleanup);

	listen(sockno, 5);
	struct sockaddr_in cli_addr;
	socklen_t clilen = sizeof(cli_addr);
	int threads = 0;

	pthread_t webui_handle;
	pthread_create(&webui_handle, NULL, run_webui, &threads);

	while (true) {
		int *args = malloc(sizeof(int) * 2);
		args[0] = accept(sockno, (struct sockaddr *) &cli_addr, &clilen);
		if (args[0] < 0) {
			perror("accept");
			return 0;
		}
		args[1] = threads++;
		pthread_t handle;
		pthread_create(&handle, NULL, handle_connection, args);
	}
}
