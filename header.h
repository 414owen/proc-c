#pragma once

#include "vect.h"

typedef struct {
	char *key;
	char *value;
} header;

declare_vect(header, header);
predeclare_vect_funcs(header, header);

vect_header parse_headers(char *str, char *end);
char* get_header(vect_header v, char *key);
