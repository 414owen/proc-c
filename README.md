# proc-c

## Requirements

* Linux
* GNU Libc
* C Compiler

## Build

```bash
make
```

## Usage

```bash
./proxy <port>

# point your proxy configs at 172.0.0.1:<port>
```

## TODO

* Appease valgrind
* Implement configuration UI
* Blocklisting
* Benchmarks
* Full W3C compliance
* Better thread management
* Better socket cleanup
