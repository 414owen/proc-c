#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "header.h"
#include "http.h"
#include "stream.h"
#include "util.h"
#include "vect.h"

char *default_port = "80";
char *default_version = "HTTP/0.9";

// parse http request line and headers into http_t
http_t parse_http(vect_char *v, char *head_end) {

	http_t res;
	res.version = default_version;
	res.http_len = v->data + 4 - head_end;
	res.data = v;

	char *afterend = v->data + v->length;

	// whitespace on request line
	char *ws1 = memchr(v->data, ' ', v->length);
	char *ws2 = memchr(ws1 + 1, ' ', afterend - ws1);
	char *ws3 = memchr(ws2, '\r', afterend - ws2);
	if (!ws2 || ws2 > ws3) ws2 = ws3;
	else res.version = extract_str(memchr(ws2, '/', ws3 - ws2) + 1, ws3 - 1);
	res.headers = parse_headers(ws3 + 2, head_end);

	// origin and port start and end
	char *o_start, *o_end, *p_start, *p_end;
	char *colon = memchr(ws1, ':', ws2 - ws1);
	res.url = extract_str(ws1 + 1, ws2 - 1);

	if (colon && *(colon + 1) == '/' && colon < ws2) o_start = colon + 3;
	else o_start = ws1 + 1;

	if ((p_start = memchr(o_start, ':', ws2 - o_start))) {
		o_end = p_start - 1;
		p_start++;
		if ((p_end = memchr(p_start, '/', ws2 - p_start))) p_end--;
		else p_end = ws2 - 1;
		res.port = extract_str(p_start, p_end);
	} else {
		if ((o_end = memchr(o_start, '/', ws2 - o_start))) o_end--;
		else o_end = ws2 - 1;
		res.port = default_port;
	}

	if (*o_start == '/') res.node = get_header(res.headers, "Host");
	else res.node = extract_str(o_start, o_end);
	res.method = extract_str(v->data, ws1 - 1);

	return res;
}

char *contains_endh(char *mem, size_t len) {
	return memmem(mem, len, "\r\n\r\n", 4);
}

// stream http up until the end of headers
http_t stream_http(int sock) {
	vect_char *v;
	char *headers_end;
	if ((headers_end = predicated_read(sock, 2, contains_endh, &v)) == NULL)
		thread_error("couldn't find end of http headers");

	return parse_http(v, headers_end);
}

void free_http_t(http_t s) {
	free(s.method);
	free(s.node);
	free(s.url);
	if (s.port != default_port) free(s.port);
	if (s.version != default_version) free(s.version);
}

void fprint_http(FILE *file, http_t http) {
	fprintf(file, "Parsed HTTP:\n"
	              "Method: %s\n"
	              "Node: %s\n"
	              "Port: %s\n"
	              "Version: %s\n"
	              "Headers:\n",
				  http.method,
				  http.node,
				  http.port,
				  http.version);
	for (size_t i = 0; i < http.headers.length; i++)
		fprintf(file, "%s | %s\n", http.headers.data[i].key,
				http.headers.data[i].value);
}

// returns true on success
const char *ok_lit = "HTTP/%s 200 OK\r\n";
bool send_okay(int dest, char *version) {
	char buf[18];
	int a = sprintf(buf, ok_lit, version);
	if (write_buffer(dest, buf, a)) return true;
	thread_error("couldn't give the okay :'(");
	return false;
}

const char *content_length = "Content-Length: %zu\r\n";
bool send_content_length(int dest, size_t len) {
	// digits in 2^64
	char buf[40];
	int a = sprintf(buf, content_length, len);
	if (write_buffer(dest, buf, a)) return true;
	thread_error("couldn't send content length");
	return false;
}

const char *redirect = "HTTP/1.1 307 Moved Temporarily\r\nLocation: %s\r\n";
bool send_redirect(int dest, char *url) {
	char *buf = malloc(strlen(redirect) + strlen(url));
	int a = sprintf(buf, redirect, url);
	if (write_buffer(dest, buf, a)) return true;
	thread_error("couldn't send redirect");
	return false;
}

bool send_end_headers(int dest) {
	if (write_buffer(dest, "\r\n", 2)) return true;
	thread_error("couldn't end headers");
	return false;
}
